---
title: 1.2. Features
geekdocCollapseSection: true
---

Software characteristics (features)

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->
