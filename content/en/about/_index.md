---
title: 1. About
geekdocCollapseSection: true
---

### Contents

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

<hr style="margin:10px 0 40px 0;">

Colmena is your digital tool box for local and community media productions. It's a 100% free of charge, secure, open source solution for mobile and desktop environments. Colmena is a software solution to create and share content, developed together with local and community media from the Global South. Colmena is self-installable free software that can be installed on its own instance.

The initiative was born as a response to the Covid-19 pandemic, led by DW Akademie and the Mexican NGO Redes por la Diversidad, Equidad y Sustentabilidad A.C. The project is supported by the German Federal Ministry of Economic development and Cooperation (BMZ) as part of the Global Crisis Initiative (GKI).  


Colmena is free to use, commonly owned and 100% open source. Is committed to a co-created and collectively owned digital infrastructure. Our community is formed by active collaborators, not just users.      
                  

<video src="https://archive.org/download/dla-21-gki-110-colmena-videographic-en-2022-04/DLA21-GKI110-Colmena_Videographic_EN_2022-04.mp4" width="100%" height="460" controls></video>

