---
title: 6.2. Translate
geekdocCollapseSection: true
---

Instructions for people who want to open a new language and how to contribute to existing ones.

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->
