---
title: Documentation 
geekdocNav: true
geekdocAlign: center
geekdocAnchor: false
geekdocBreadcrumb: false


---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 

<span class="badge-placeholder">[![Build Status](https://img.shields.io/drone/build/thegeeklab/hugo-geekdoc?logo=drone&server=https%3A%2F%2Fdrone.thegeeklab.de)](https://drone.thegeeklab.de/thegeeklab/hugo-geekdoc)</span>
<span class="badge-placeholder">[![Hugo Version](https://img.shields.io/badge/hugo-0.93-blue.svg)](https://gohugo.io)</span>
<span class="badge-placeholder">[![GitHub release](https://img.shields.io/github/v/release/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/releases/latest)</span>
<span class="badge-placeholder">[![GitHub contributors](https://img.shields.io/github/contributors/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/graphs/contributors)</span>
<span class="badge-placeholder">[![License: MIT](https://img.shields.io/github/license/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/blob/main/LICENSE)</span>
  -->
  
<!-- markdownlint-restore -->

Colmena is a software solution to create and share content, developed together with local and community media from the Global South. Colmena is free software self-hosted that can be installed on its own instance.
                          

<!-- {{< button size="large" relref="_includes/" class="buttom-custom" >}}Explore contents{{< /button >}} -->

<hr style="margin:10px 0 40px 0;">


## Sections

{{< columns >}}


### [Deploy an instance](deploy/)

Documentation for DevOps on how to deploy Colmena.

<--->

### [Manage an instance](manage/)

Superadmin Backend Tool manual to manage instances, open accounts for Coordinators, invite media...

<--->

### [Use Colmena](use/)

Manual for end users of the PWA.

{{< /columns >}}

{{< columns >}}


### [About Colmena](about/)

Brief description of the project and the application.

<--->

### [Manual for training](training/)

A manual to train journalists and communicators in the use of Colmena. 

<--->

### [Contribute](contribute/)

Develop, translate or document.

{{< /columns >}}
