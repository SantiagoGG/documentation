---
title: 1. Sobre Colmena
geekdocCollapseSection: true
---

### Contents

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

<hr style="margin:10px 0 40px 0;">

Colmena es tu caja de herramientas digital para producciones radiofónicas y contenidos locales y comunitarias. 

Colmena apuesta por una infraestructura digital co-creada y de propiedad colectiva. Nuestra comunidad está formada por colaboradores activos, no solo por usuarios.  

Colmena es una solución de software para crear y compartir contenidos, desarrollada junto con medios locales y comunitarios del Sur Global. Colmena es de uso gratuito, de propiedad común y de código abierto al 100%.

La iniciativa nació como respuesta a la pandemia de Covid-19, liderada por DW Akademie y la ONG mexicana Redes por la Diversidad, Equidad y Sustentabilidad A.C. El proyecto cuenta con el apoyo del Ministerio Federal de Desarrollo Económico y Cooperación de Alemania (BMZ) en el marco de la Iniciativa de Crisis Global (GKI).
                  

<video src="https://archive.org/download/dla-21-gki-110-colmena-videographic-es-2022-04/DLA21-GKI110-Colmena_Videographic_ES_2022-04.mp4" width="100%" height="460" controls></video>

