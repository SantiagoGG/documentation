---
title: 2.2. Pasos y configuraciones
geekdocCollapseSection: true
---

Un detallado instructivo para instalar Colmena.

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->
