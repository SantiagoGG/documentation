---
title: 2.4. Proveedores de Colmena
geekdocCollapseSection: true
---

Si no quieres encargarte de instalar tu propia instancia de Colmena, puedes contratar una con nuestros proveedores aliados. 

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->
