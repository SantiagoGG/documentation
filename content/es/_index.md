---
title: Documentación 
geekdocNav: true
geekdocAlign: center
geekdocAnchor: false
geekdocBreadcrumb: false


---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 

<span class="badge-placeholder">[![Build Status](https://img.shields.io/drone/build/thegeeklab/hugo-geekdoc?logo=drone&server=https%3A%2F%2Fdrone.thegeeklab.de)](https://drone.thegeeklab.de/thegeeklab/hugo-geekdoc)</span>
<span class="badge-placeholder">[![Hugo Version](https://img.shields.io/badge/hugo-0.93-blue.svg)](https://gohugo.io)</span>
<span class="badge-placeholder">[![GitHub release](https://img.shields.io/github/v/release/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/releases/latest)</span>
<span class="badge-placeholder">[![GitHub contributors](https://img.shields.io/github/contributors/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/graphs/contributors)</span>
<span class="badge-placeholder">[![License: MIT](https://img.shields.io/github/license/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/blob/main/LICENSE)</span>
  -->
  
<!-- markdownlint-restore -->

Colmena es una solución para crear y compartir contenidos, creada conjuntamente con medios locales y comunitarios del Sur Global. Colmena es software libre auto instalable que puede ser desplegado en servidores propios. 
                          

<!-- {{< button size="large" relref="_includes/" class="buttom-custom" >}}Explora los contenidos{{< /button >}}      -->

<hr style="margin:10px 0 40px 0;">


## Secciones

{{< columns >}}


### [Instala una instancia](deploy/)

Documentación para DevOps sobre como hacer el deplyment de Colmena en un servidor.

<--->

### [Gestiona una instancia](manage/)

Aprende a usar el Superadmin Backend Tool, la herramienta de Colmena para administrar la instancia: abrir cuentas para Coordinadores, invitar a medios...

<--->

### [Usa Colmena](use/)

Manual para periodistas y comunicadores que quieren usar la PWA Colmena. 

{{< /columns >}}

{{< columns >}}


### [Sobre Colmena](about/)

Una breve descripción del proyecto y la aplicación. 

<--->

### [Manual para capacitaciones](training/)

Un manual para capacitar a periodistas y comunicadores en el uso de Colmena.  

<--->

### [Contribuye](contribute/)

Desarrolla, traduce o documenta, contribuye para que el proyecto crezca.

{{< /columns >}}
