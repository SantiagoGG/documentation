---
title: 6.2. Traducir
geekdocCollapseSection: true
---

Instrucciones para personas que quieran abrir un nuevo idioma y cómo contribuir a los existentes.

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->
