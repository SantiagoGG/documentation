# Colmena documentation

General documentation for the colmena project.


## Dependencies

To start the project you need to install **the last version** of [hugo](https://gohugo.io/installation/linux/)

## Run locally

```
hugo server -D

```


Seguir instrucciones: https://docs.gitlab.com/ee/tutorials/hugo/#deploy-and-view-your-hugo-site

1.- Install Hugo https://gohugo.io/installation/linux/

2.- Instalar tema: https://geekdocs.de/usage/getting-started/

3.- Desarrollar: 
